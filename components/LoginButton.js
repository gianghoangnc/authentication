import React, {Component} from 'react';
import {LoginButton, AccessToken, LoginManager} from 'react-native-fbsdk';
import {TouchableOpacity, Text, View} from 'react-native';

export default class LogIn extends Component {

  render() {
    return (
        <View>
          <LoginButton
            publishPermissions={["public_profile"]}
            onLoginFinished={
              (error, result) => {
                if (error) {
                  alert("login has error: " + result.error);
                } else if (result.isCancelled) {
                  alert("login is cancelled.");
                } else {
                  AccessToken.getCurrentAccessToken().then(
                    (data) => {
                      alert(data.accessToken.toString())
                    }
                  )
                }
              }
            }
            onLogoutFinished={() => alert("logout.")}/>
        </View>
      );
  }
}

LoginManager.logInWithReadPermissions(['email']).then(
  function(result) {
    if (result.isCancelled) {
      alert('Login cancelled');
    } else {
      alert('Login success with permissions: '
        +result.grantedPermissions.toString());
    }
  },
  function(error) {
    alert('Login fail with error: ' + error);
  }
);